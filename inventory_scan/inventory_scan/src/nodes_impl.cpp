// Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause-Clear

#include <geometry_msgs/msg/quaternion.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2/utils.h>

#include <geometry_msgs/msg/vector3.hpp>
#include <opencv2/opencv.hpp>

#include "geometry_msgs/msg/pose.hpp"
#include "nav_msgs/msg/occupancy_grid.hpp"
#include "nodes_def.hpp"

using namespace std::placeholders;
using namespace std::chrono_literals;

namespace qrb_ros::sample::inventory_scan
{
// --------------- Class PathManagerNode ---------------------------- //
PathManagerNode::PathManagerNode(std::shared_ptr<PathManager> path,
  std::shared_ptr<NaviState> navi_state)
  : rclcpp::Node("path_node"), path_manager_(path), navi_state_(navi_state)
{
  callback_group_ = this->create_callback_group(rclcpp::CallbackGroupType::Reentrant);
  server_ = this->create_service<PathService>(
      "scan_path", std::bind(&PathManagerNode::service_callback, this, _1, _2, _3),
      rmw_qos_profile_services_default, callback_group_);

  vp_client_ = this->create_client<VirtualPath>("virtual_path");
  tf_buffer_ = std::make_unique<tf2_ros::Buffer>(this->get_clock());
  tf_listener_ = std::make_shared<tf2_ros::TransformListener>(*tf_buffer_);

  timer_ = this->create_wall_timer(2s, std::bind(&PathManagerNode::init, this), callback_group_);
}

void PathManagerNode::init()
{
  if (!vp_client_->wait_for_service(std::chrono::seconds(1))) {
    RCLCPP_INFO(this->get_logger(), "wait for virtual path service avaliable..");
    return;
  }

  timer_->cancel();
  std::vector<uint32_t> list;
  size_t len;
  init_station_list(list, len);
  init_station(list, len);
  init_path(list, len);
}

void PathManagerNode::service_callback(const std::shared_ptr<rmw_request_id_t> request_header,
    const std::shared_ptr<PathService::Request> request,
    std::shared_ptr<PathService::Response> response)
{
  (void)request_header;

  bool result;
  uint32_t id;
  Point2D p{ request->x, request->y, request->angle };
  Point2D cur_p;

  if (navi_state_->get_state() == NaviState::NAVIGATING) {
    response->result = false;
    return;
  }

  uint32_t cmd = request->cmd_id;
  switch (cmd) {
    case PathService::Request::ADD_STATION:
      id = virtual_path_add_station(p);
      if (id != 0)  // id == 0 is invalid
        result = path_manager_->add_station(id, p);
      else
        result = false;
      break;

    case PathService::Request::DELETE_STATION:
      if (request->stations[0] != 0) {
        result = virtual_path_delete_station(request->stations[0]);
        path_manager_->delete_station(request->stations[0]);
      } else
        result = false;
      break;

    case PathService::Request::QUERY_POSE:
      result = query_pose(cur_p);
      if (result) {
        response->x = cur_p.x;
        response->y = cur_p.y;
        response->angle = cur_p.angle;
      }
      break;

    case PathService::Request::ADD_PATH:
      if (request->stations[0] != 0 && request->stations[1] != 0) {
        virtual_path_add_path(request->stations[0], request->stations[1]);
        result = path_manager_->add_path(request->stations[0], request->stations[1]);
      } else
        result = false;
      break;

    case PathService::Request::DELETE_PATH:
      if (request->stations[0] != 0 && request->stations[1] != 0) {
        virtual_path_delete_path(request->stations[0], request->stations[1]);
        result = path_manager_->delete_path(request->stations[0], request->stations[1]);
      } else
        result = false;
      break;

    case PathService::Request::ADD_FULL_PATH:
      if (request->stations.size() > 1) {
        result = path_manager_->add_full_path(request->stations);
      } else
        result = false;
      break;

    default:
      result = false;
      break;
  }
  response->result = result;
}

void PathManagerNode::init_station_list(std::vector<uint32_t>& list, size_t& len)
{
  // get Station List from Server
  auto request = std::make_unique<VirtualPath::Request>();
  request->api_id = 17;  // TODO api_id needs a MACRO
  request->waypoint_id = 0;

  std::vector<uint32_t> result_list;
  auto response_callback = [this, &result_list](VirtualPathFuture future) {
    std::unique_lock<std::mutex> lck(lock_);
    auto response = future.get();
    vp_response_ret_ = response->result;

    result_list = std::vector<uint32_t>(response->waypoint_id_list.cbegin(),
      response->waypoint_id_list.cend());
    cv_.notify_one();
  };

  vp_client_->async_send_request(std::move(request), response_callback);
  std::unique_lock<std::mutex> lck(lock_);
  cv_.wait(lck);

  RCLCPP_INFO(this->get_logger(), "Virual Path: init station list done");

  if (vp_response_ret_ == 0) {
    len = 0;
    return;
  }

  len = result_list.size();
  if (len == 0)
    return;

  list = std::vector<uint32_t>(result_list.cbegin(), result_list.cend());
}

void PathManagerNode::init_station(const std::vector<uint32_t>& list, size_t len)
{
  if (len == 0)
    return;

  Point2D p;
  auto response_callback = [this, &p](VirtualPathFuture future) {
    std::unique_lock<std::mutex> lck(lock_);
    auto response = future.get();
    vp_response_ret_ = response->result;

    p.x = response->waypoint.x;
    p.y = response->waypoint.y;
    p.angle = response->waypoint.z;
    cv_.notify_one();
  };

  for (size_t idx = 0; idx < len; idx++) {
    // get Station List from Server
    auto request = std::make_unique<VirtualPath::Request>();
    request->api_id = 18;  // TODO api_id needs a MACRO
    request->waypoint_id = list[idx];

    vp_client_->async_send_request(std::move(request), response_callback);
    {
      std::unique_lock<std::mutex> lck(lock_);
      cv_.wait(lck);
    }

    if (vp_response_ret_ == 0) {
      continue;
    }

    path_manager_->add_station(list[idx], p);
  }

  RCLCPP_INFO(this->get_logger(), "Virual Path: init station done");
}

void PathManagerNode::init_path(const std::vector<uint32_t>& list, size_t len)
{
  if(len == 0)
    return;

  std::vector<uint32_t> connections;
  auto response_callback = [this, &connections](VirtualPathFuture future) {
    std::unique_lock<std::mutex> lck(lock_);
    auto response = future.get();
    vp_response_ret_ = response->result;
    connections = std::vector<uint32_t>(response->adjacent_waypoints.cbegin(),
                                        response->adjacent_waypoints.cend());
    cv_.notify_one();
  };

  for(size_t idx = 0; idx < len; idx++) {
    //get Station List from Server
    auto request = std::make_unique<VirtualPath::Request>();
    request->api_id = 21;  // TODO api_id needs a MACRO
    request->waypoint_id = list[idx];

    vp_client_->async_send_request(std::move(request), response_callback);
    {
      std::unique_lock<std::mutex> lck(lock_);
      cv_.wait(lck);
    }

    if(vp_response_ret_ == 0) {
      continue;
    }

    size_t num = connections.size();
    RCLCPP_INFO(this->get_logger(), "Path: init path %u with %lu connections", list[idx], num);

    if(num == 0)
      continue;

    for(size_t i = 0; i < num; i++) {
      path_manager_->add_path(list[idx], connections[i]);
    }
  }

  RCLCPP_INFO(this->get_logger(), "Virual Path: init path done");
}

uint32_t PathManagerNode::virtual_path_add_station(const Point2D & pos)
{
  geometry_msgs::msg::Vector3 vector;
  vector.x = pos.x;
  vector.y = pos.y;
  vector.z = pos.angle;

  auto request = std::make_unique<VirtualPath::Request>();
  request->api_id = 15;  // TODO api_id needs a MACRO
  request->waypoint_id = 0;
  request->waypoint = vector;

  uint32_t id = 0;
  auto vp_response_callback = [this, &id](VirtualPathFuture future) {
    std::unique_lock<std::mutex> lck(lock_);
    auto response = future.get();
    vp_response_ret_ = response->result;
    id = response->waypoint_id;
    cv_.notify_one();
  };

  vp_client_->async_send_request(std::move(request), std::move(vp_response_callback));
  std::unique_lock<std::mutex> lck(lock_);
  cv_.wait(lck);

  RCLCPP_INFO(this->get_logger(), "Path: add station %u", id);

  return id;
}

bool PathManagerNode::virtual_path_delete_station(uint32_t id)
{
  auto request = std::make_unique<VirtualPath::Request>();
  request->api_id = 16;  // TODO api_id needs a MACRO
  request->waypoint_id = id;

  auto response_callback = [this](VirtualPathFuture future) {
    std::unique_lock<std::mutex> lck(lock_);
    auto response = future.get();
    vp_response_ret_ = response->result;

    cv_.notify_one();
  };
  vp_client_->async_send_request(std::move(request), response_callback);
  std::unique_lock<std::mutex> lck(lock_);
  cv_.wait(lck);

  RCLCPP_INFO(this->get_logger(), "Path: delete station %u", id);

  if (vp_response_ret_ > 0)
    return true;
  else
    return false;
}

bool PathManagerNode::query_pose(Point2D & pos)
{
  geometry_msgs::msg::TransformStamped cur_pose;
  try {
    cur_pose = tf_buffer_->lookupTransform("map", "base_link",
      tf2::TimePoint(std::chrono::nanoseconds(0)));
  } catch (tf2::TransformException & ex) {
    RCLCPP_ERROR(this->get_logger(), "Error querying transform: %s", ex.what());
    return false;
  }

  tf2::Quaternion tf_quaternion;
  tf_quaternion.setX(cur_pose.transform.rotation.x);
  tf_quaternion.setY(cur_pose.transform.rotation.y);
  tf_quaternion.setZ(cur_pose.transform.rotation.z);
  tf_quaternion.setW(cur_pose.transform.rotation.w);

  pos.x = cur_pose.transform.translation.x;
  pos.y = cur_pose.transform.translation.y;
  pos.angle = tf2::getYaw(tf_quaternion);

  return true;
}

bool PathManagerNode::virtual_path_add_path(uint32_t id1, uint32_t id2)
{
  auto request = std::make_unique<VirtualPath::Request>();
  request->api_id = 19;  // TODO api_id needs a MACRO
  request->waypoint_id = id1;
  request->adjacent_waypoints.push_back(id2);

  auto response_callback = [this](VirtualPathFuture future) {
    std::unique_lock<std::mutex> lck(lock_);
    auto response = future.get();
    vp_response_ret_ = response->result;

    cv_.notify_one();
  };
  vp_client_->async_send_request(std::move(request), response_callback);
  std::unique_lock<std::mutex> lck(lock_);
  cv_.wait(lck);

  RCLCPP_INFO(this->get_logger(), "Path: add path %u --> %u", id1, id2);

  if (vp_response_ret_ > 0)
    return true;
  else
    return false;
}

bool PathManagerNode::virtual_path_delete_path(uint32_t id1, uint32_t id2)
{
  auto request = std::make_unique<VirtualPath::Request>();
  request->api_id = 20;  // TODO api_id needs a MACRO
  request->waypoint_id = id1;
  request->adjacent_waypoints.push_back(id2);

  auto response_callback = [this](VirtualPathFuture future) {
    std::unique_lock<std::mutex> lck(lock_);
    auto response = future.get();
    vp_response_ret_ = response->result;

    cv_.notify_one();
  };
  vp_client_->async_send_request(std::move(request), response_callback);
  std::unique_lock<std::mutex> lck(lock_);
  cv_.wait(lck);

  RCLCPP_INFO(this->get_logger(), "Path: delete path %u --> %u", id1, id2);

  if (vp_response_ret_ > 0)
    return true;
  else
    return false;
}

// --------------- Class ScanNode ---------------------------- //
ScanNode::ScanNode(std::shared_ptr<SyncState> sync_state,
  std::shared_ptr<GoodsManager> goods)
  : rclcpp::Node("scan_node"), sync_state_(sync_state), goods_(goods)
{
  client_ = rclcpp_action::create_client<VisionAction>(this, "qr_case");

  if (!client_->wait_for_action_server(std::chrono::seconds(1))) {
    RCLCPP_ERROR(this->get_logger(), "waiting vision action server available..");
  }

  std::thread{ std::bind(&ScanNode::execute, this) }.detach();
}

void ScanNode::execute(void)
{
  while(sync_state_->get_state() != SyncState::RELEASE) {
    sync_state_->reset();

    RCLCPP_INFO(this->get_logger(), "scan wait start..");
    sync_state_->wait_start();
    if(sync_state_->get_state() == SyncState::START) {
      RCLCPP_INFO(this->get_logger(), "scan starts");
      auto goal_msg = std::make_shared<VisionAction::Goal>();
      goal_msg->action_id = VisionAction::Goal::REGISTER_CAMERA_QR_DETECT;
      goal_msg->camera_id = 1; //hardcode

      auto send_goal_options = rclcpp_action::Client<VisionAction>::SendGoalOptions();
      send_goal_options.goal_response_callback =
        std::bind(&ScanNode::goal_response_callback, this, _1);
      send_goal_options.feedback_callback = std::bind(&ScanNode::feedback_callback, this, _1, _2);
      send_goal_options.result_callback = std::bind(&ScanNode::result_callback, this, _1);

      client_->async_send_goal(*goal_msg, send_goal_options);
    }

    RCLCPP_INFO(this->get_logger(), "scan wait stop");
    sync_state_->wait_stop();
    if(sync_state_->get_state() == SyncState::STOP) {
      if(goal_handle_ != nullptr)
        client_->async_cancel_goal(goal_handle_);
    }
  }
}

void ScanNode::goal_response_callback(GoalHandleVision::SharedPtr goal_handle)
{
  if (!goal_handle) {
    RCLCPP_ERROR(this->get_logger(), "qr code goal was rejected by server");
    goal_handle_ = nullptr;
  } else {
    RCLCPP_INFO(this->get_logger(), "qr code goal accepted by server. nice!");
    goal_handle_ = goal_handle;
  }
}

void ScanNode::feedback_callback(GoalHandleVision::SharedPtr,
    const std::shared_ptr<const VisionAction::Feedback> feedback)
{
  size_t len = feedback->contents.size();
  size_t idx;
  std::string msg;

  RCLCPP_INFO_STREAM(this->get_logger(), "received QR code feedback. contents len:" << len);

  if(len > 0) {
    for(idx = 0; idx < len; ++idx) {
      if(feedback->contents[idx].empty())
        continue;

      // QR_ID, name:xxx, q:num [first: last, last:]
      // shelf:ID, name:xxx [first: first, last:]
      std::string::size_type label_s = feedback->contents[idx].find_first_of(":");
      std::string::size_type label_e = feedback->contents[idx].find_first_of(",");
      bool is_goods;
      if (label_e >= label_s) {
        is_goods = false;
        RCLCPP_INFO_STREAM(this->get_logger(), "process qr code msg: shelf string");
      } else {
        is_goods = true;
        RCLCPP_INFO_STREAM(this->get_logger(), "process qr code msg: goods string");
      }

      RCLCPP_INFO_STREAM(this->get_logger(), "process qr code msg:" << feedback->contents[idx]);
      goods_->process_msg(feedback->contents[idx], is_goods);
    }
  }
}

void ScanNode::result_callback(const GoalHandleVision::WrappedResult & result)
{
  switch (result.code) {
    case rclcpp_action::ResultCode::SUCCEEDED:
      break;
    case rclcpp_action::ResultCode::ABORTED:
      RCLCPP_ERROR(this->get_logger(), "goal was aborted");
      return;
    case rclcpp_action::ResultCode::CANCELED:
      RCLCPP_ERROR(this->get_logger(), "goal was canceled");
      return;
    default:
      RCLCPP_ERROR(this->get_logger(), "unknown result code");
      return;
  }

  RCLCPP_INFO_STREAM(
      this->get_logger(), "received result, result: ");  // << result.result->result);
}

// --------------- Class NaviNode ---------------------------- //
NaviNode::NaviNode(std::shared_ptr<NaviState> navi_state, std::shared_ptr<PathManager> path,
  std::shared_ptr<SyncState> sync_state)
  : Node("navi_node"), navi_state_(navi_state), path_(path), sync_state_(sync_state),navi_server_ready_(false)
{
  callback_group_ = this->create_callback_group(rclcpp::CallbackGroupType::Reentrant);
  navi_server_ = this->create_service<NaviService>(
      "scan_control", std::bind(&NaviNode::service_callback, this, _1, _2, _3),
      rmw_qos_profile_services_default, callback_group_);

  navi_client_ = rclcpp_action::create_client<NavigateToPose>(this, "cmd");

  timer_ = this->create_wall_timer(1s, std::bind(&NaviNode::init, this), callback_group_);
}

void NaviNode::init()
{
  //request navigation server
  if (!navi_client_->wait_for_action_server(std::chrono::seconds(1))) {
    RCLCPP_ERROR(this->get_logger(), "amr action server is not available");
    return;
  }

  navi_server_ready_ = true;
  timer_->cancel();
}

void NaviNode::service_callback(const std::shared_ptr<rmw_request_id_t> request_header,
    const std::shared_ptr<NaviService::Request> request,
    std::shared_ptr<NaviService::Response> response)
{
  (void)request_header;
  RCLCPP_INFO(this->get_logger(), "navi service received call");
  uint32_t result;

  std::vector<uint32_t> full_path;

  uint32_t cmd = request->cmd_id;
  switch (cmd) {
    case NaviService::Request::START_SCAN:
      path_->get_full_path(full_path);
      if (full_path.size() < 2)  // not enough stations
        result = NaviService::Response::NAVI_ERROR;
      else if(navi_state_->get_state() != NaviState::IDLE){
        result = NaviService::Response::NAVI_NOT_READY;
      }
      else {
        if (!navi_server_ready_) {
          RCLCPP_ERROR(this->get_logger(), "amr action server is not available");
          result = NaviService::Response::NAVI_NOT_READY;
        } else {
          result = NaviService::Response::SCANNING;
          navi_state_->set_state(NaviState::NAVIGATING);
          //start navigation
          std::thread{ std::bind(&NaviNode::execute, this, _1), full_path }.detach();
        }
      }
      break;

    case NaviService::Request::STOP_SCAN:
      if(navi_state_->get_state() == NaviState::NAVIGATING){
        result = NaviService::Response::SCAN_STOPPED;
        navi_state_->set_state(NaviState::GOING_HOME);
        if(goal_handle_ != nullptr)
          navi_client_->async_cancel_goal(goal_handle_);
        //going home
        std::thread{ std::bind(&NaviNode::go_home, this) }.detach();
      } else
        result = false;
      break;

    default:
      result = false;
      break;
  }
  response->result = result;
}

void NaviNode::go_home()
{
  RCLCPP_INFO(this->get_logger(), "inventory scan navi going home...");

  auto pose_msg = geometry_msgs::msg::PoseStamped();
  pose_msg.header.stamp = this->now();
  pose_msg.header.frame_id = "world";

  //home
  pose_msg.pose.position.x = 0.0;
  pose_msg.pose.position.y = 0.0;
  pose_msg.pose.position.z = 0.0;

  tf2::Quaternion q;
  q.setRPY(0.0, 0.0, 0.0);

  pose_msg.pose.orientation.x = q.x();
  pose_msg.pose.orientation.y = q.y();
  pose_msg.pose.orientation.z = q.z();
  pose_msg.pose.orientation.w = q.w();

  auto goal_msg = std::make_shared<NavigateToPose::Goal>();
  goal_msg->command = 3; //Command::P2PNAV;
  goal_msg->goal = pose_msg;

  auto send_goal_options = rclcpp_action::Client<NavigateToPose>::SendGoalOptions();
  send_goal_options.goal_response_callback =
      std::bind(&NaviNode::navi_goal_response_callback, this, _1);
  send_goal_options.feedback_callback = std::bind(&NaviNode::navi_feedback_callback, this, _1, _2);
  send_goal_options.result_callback = std::bind(&NaviNode::navi_result_callback, this, _1);

  navi_client_->async_send_goal(*goal_msg, send_goal_options);
  navi_result_ = NaviNode::NAVI_START;
  //wait finish
  {
    std::unique_lock<std::mutex> lck(lock_);
    while (navi_result_ == NaviNode::NAVI_START) { cv_.wait(lck); }
  }

  if (navi_result_ == NaviNode::NAVI_ERROR) {
    navi_state_->set_state(NaviState::NAVI_ERR);
    return;
  }

  navi_state_->set_state(NaviState::IDLE);
}

void NaviNode::execute(const std::vector<uint32_t>& full_path)
{
  RCLCPP_INFO(this->get_logger(), "inventory scan starts navigation...");

  //send 1st goal, p2p navi
  Point2D p;
  path_->get_station(full_path[0], p);
  auto pose_msg = geometry_msgs::msg::PoseStamped();
  pose_msg.header.stamp = this->now();
  pose_msg.header.frame_id = "world";

  pose_msg.pose.position.x = p.x;
  pose_msg.pose.position.y = p.y;
  pose_msg.pose.position.z = 0.0;

  tf2::Quaternion q;
  q.setRPY(0.0, 0.0, p.angle);

  pose_msg.pose.orientation.x = q.x();
  pose_msg.pose.orientation.y = q.y();
  pose_msg.pose.orientation.z = q.z();
  pose_msg.pose.orientation.w = q.w();

  auto goal_msg = std::make_shared<NavigateToPose::Goal>();
  goal_msg->command = 3; //Command::P2PNAV;
  goal_msg->goal = pose_msg;

  auto send_goal_options = rclcpp_action::Client<NavigateToPose>::SendGoalOptions();
  send_goal_options.goal_response_callback =
      std::bind(&NaviNode::navi_goal_response_callback, this, _1);
  send_goal_options.feedback_callback = std::bind(&NaviNode::navi_feedback_callback, this, _1, _2);
  send_goal_options.result_callback = std::bind(&NaviNode::navi_result_callback, this, _1);

  navi_client_->async_send_goal(*goal_msg, send_goal_options);
  navi_result_ = NaviNode::NAVI_START;
  //wait finish
  {
    std::unique_lock<std::mutex> lck(lock_);
    while (navi_result_ == NaviNode::NAVI_START) { cv_.wait(lck); }
  }

  if (navi_result_ == NaviNode::NAVI_ERROR) {
    navi_state_->set_state(NaviState::NAVI_ERR);
    return;
  }

  //start scan
  sync_state_->start();

  //send 2nd goal. follow path
  goal_msg = std::make_shared<NavigateToPose::Goal>();
  goal_msg->command = 6; //Command::FOLLOW_PATH;
  goal_msg->goal_id = full_path[full_path.size() - 1];
  for (size_t idx = 0; idx < full_path.size() - 1; idx++) {
    goal_msg->passing_waypoint_ids.push_back(full_path[idx]);
  }

  send_goal_options = rclcpp_action::Client<NavigateToPose>::SendGoalOptions();
  send_goal_options.goal_response_callback =
      std::bind(&NaviNode::navi_goal_response_callback, this, _1);
  send_goal_options.feedback_callback = std::bind(&NaviNode::navi_feedback_callback, this, _1, _2);
  send_goal_options.result_callback = std::bind(&NaviNode::navi_result_callback, this, _1);

  navi_client_->async_send_goal(*goal_msg, send_goal_options);
  navi_result_ = NaviNode::NAVI_START;

  //waiting for 2nd pahse (follow path)
  {
    std::unique_lock<std::mutex> lck(lock_);
    while (navi_result_ == NaviNode::NAVI_START) { cv_.wait(lck); }
  }

  sync_state_->stop();

  if (navi_result_ == NaviNode::NAVI_ERROR) {
    navi_state_->set_state(NaviState::NAVI_ERR);
    return;
  }

  //send 3rd goal, going home
  pose_msg = geometry_msgs::msg::PoseStamped();
  pose_msg.header.stamp = this->now();
  pose_msg.header.frame_id = "world";

  //home
  pose_msg.pose.position.x = 0.0;
  pose_msg.pose.position.y = 0.0;
  pose_msg.pose.position.z = 0.0;

  q.setRPY(0.0, 0.0, 0.0);

  pose_msg.pose.orientation.x = q.x();
  pose_msg.pose.orientation.y = q.y();
  pose_msg.pose.orientation.z = q.z();
  pose_msg.pose.orientation.w = q.w();

  goal_msg = std::make_shared<NavigateToPose::Goal>();
  goal_msg->command = 3; //Command::P2PNAV;
  goal_msg->goal = pose_msg;


  send_goal_options = rclcpp_action::Client<NavigateToPose>::SendGoalOptions();
  send_goal_options.goal_response_callback =
      std::bind(&NaviNode::navi_goal_response_callback, this, _1);
  send_goal_options.feedback_callback = std::bind(&NaviNode::navi_feedback_callback, this, _1, _2);
  send_goal_options.result_callback = std::bind(&NaviNode::navi_result_callback, this, _1);

  navi_client_->async_send_goal(*goal_msg, send_goal_options);
  navi_result_ = NaviNode::NAVI_START;
  //wait finish
  {
    std::unique_lock<std::mutex> lck(lock_);
    while (navi_result_ == NaviNode::NAVI_START) { cv_.wait(lck); }
  }

  if (navi_result_ == NaviNode::NAVI_ERROR) {
    navi_state_->set_state(NaviState::NAVI_ERR);
    return;
  }

  navi_state_->set_state(NaviState::IDLE);
}

void NaviNode::navi_goal_response_callback(GoalHandleNavigate::SharedPtr goal_handle)
{
  if (!goal_handle) {
    RCLCPP_ERROR(this->get_logger(), "navi goal was rejected by server");
    goal_handle_ = nullptr;
    {
      std::unique_lock<std::mutex> lck(lock_);
      navi_result_ = NaviNode::NAVI_ERROR;
      cv_.notify_all();
    }
  } else {
    RCLCPP_INFO(this->get_logger(), "navi goal accepted by server!");
    goal_handle_ = goal_handle;
  }
}

void NaviNode::navi_feedback_callback(GoalHandleNavigate::SharedPtr,
    const std::shared_ptr<const NavigateToPose::Feedback> feedback)
{
  RCLCPP_INFO_STREAM(this->get_logger(), "navi received feedback.");
}

void NaviNode::navi_result_callback(const GoalHandleNavigate::WrappedResult & result)
{
  switch (result.code) {
    case rclcpp_action::ResultCode::SUCCEEDED:
      {
        std::unique_lock<std::mutex> lck(lock_);
        navi_result_ = NaviNode::NAVI_FINISH;
        cv_.notify_all();
      }
      break;
    case rclcpp_action::ResultCode::ABORTED:
      RCLCPP_ERROR(this->get_logger(), "goal was aborted");
      {
        std::unique_lock<std::mutex> lck(lock_);
        navi_result_ = NaviNode::NAVI_ERROR;
        cv_.notify_all();
      }
      return;
    case rclcpp_action::ResultCode::CANCELED:
      RCLCPP_ERROR(this->get_logger(), "goal was canceled");
      return;
    default:
      RCLCPP_ERROR(this->get_logger(), "unknown result code");
      return;
  }

  RCLCPP_INFO_STREAM(this->get_logger(), "received result, result: " << result.result->result);
}

// --------------- Class PubNode ---------------------------- //
PubNode::PubNode(std::shared_ptr<MapManager> map_manager, std::shared_ptr<NaviState> navi_state,
  std::shared_ptr<GoodsManager> goods)
  : Node("pub_node"), map_manager_(map_manager), navi_state_(navi_state), goods_(goods)
{
  result_pub_ = this->create_publisher<inventory_scan_msgs::msg::ScanResult>("/scan_result", 10);
  img_pub_ = this->create_publisher<sensor_msgs::msg::CompressedImage>("/scan_map", 5);

  timer_ = this->create_wall_timer(std::chrono::seconds(1), std::bind(&PubNode::timer_cb, this));
}

void PubNode::publish_msgs()
{
  //publish goods info
  auto msg = std::make_unique<inventory_scan_msgs::msg::ScanResult>();

  std::vector<struct GoodsInfo> goods;
  std::vector<std::string> warn_msg;

  goods_->get_goods_info(goods, warn_msg);

  size_t max_idx = goods.size();
  //RCLCPP_INFO_STREAM(this->get_logger(), "public scan result, goods info: " << max_idx);
  size_t idx;
  if(max_idx > 0) {
     msg->goods_info.resize(max_idx);
    for(idx = 0; idx < max_idx; ++idx) {
      msg->goods_info[idx] = "Goods: " + goods[idx].name + ", quantities: " +
        std::to_string(goods[idx].quantity);
    }
  }

  max_idx = warn_msg.size();
  //RCLCPP_INFO_STREAM(this->get_logger(), "public scan result, warning info: " << max_idx);
  if(max_idx > 0) {
    msg->warnings.resize(max_idx);
    for(idx = 0; idx < max_idx; ++idx) {
      msg->warnings[idx] = warn_msg[idx];
    }
  }
  result_pub_->publish(*msg);

  //publish map
  cv::Mat image;
  cv::Mat image2;
  bool has_map = false;
  has_map = map_manager_->draw_map(image);
  if(has_map && image.cols < 400 && image.rows < 400){
    cv::resize(image,image2,cv::Size(),2, 2);
    image = image2;
  }

  if (has_map) {
    // Convert the image to a compressed format PNG
    std::unique_ptr<sensor_msgs::msg::CompressedImage> compressed_image =
        std::make_unique<sensor_msgs::msg::CompressedImage>();
    compressed_image->header.stamp = this->now();
    compressed_image->format = "png";
    cv_bridge::CvImage(std_msgs::msg::Header(), "bgr8", image)
        .toCompressedImageMsg(*compressed_image);

    img_pub_->publish(*compressed_image);
  }
}

void PubNode::timer_cb()
{
  publish_msgs();
}

// --------------- Class SubNode ---------------------------- //
SubNode::SubNode(std::shared_ptr<MapManager> map_manager)
  : Node("sub_node"), map_manager_(map_manager)
{
  tf_buffer_ = std::make_unique<tf2_ros::Buffer>(this->get_clock());
  tf_listener_ = std::make_shared<tf2_ros::TransformListener>(*tf_buffer_);

  tf_timer_ = this->create_wall_timer(
      std::chrono::milliseconds(500), std::bind(&SubNode::tf_timer_cb, this));

  map_listener_ = this->create_subscription<nav_msgs::msg::OccupancyGrid>(
      "/map", 10, std::bind(&SubNode::map_cb, this, std::placeholders::_1));
}

void SubNode::tf_timer_cb()
{
  geometry_msgs::msg::TransformStamped cur_pose;
  Point2D pos;
  try {
    cur_pose = tf_buffer_->lookupTransform("map", "base_link",
      tf2::TimePoint(std::chrono::nanoseconds(0)));

  } catch (tf2::TransformException& ex) {
    RCLCPP_ERROR(this->get_logger(), "Error querying transform: %s", ex.what());
    return;
  }

  tf2::Quaternion tf_quaternion;
  tf_quaternion.setX(cur_pose.transform.rotation.x);
  tf_quaternion.setY(cur_pose.transform.rotation.y);
  tf_quaternion.setZ(cur_pose.transform.rotation.z);
  tf_quaternion.setW(cur_pose.transform.rotation.w);

  pos.x = cur_pose.transform.translation.x;
  pos.y = cur_pose.transform.translation.y;
  pos.angle = tf2::getYaw(tf_quaternion);

  map_manager_->set_pose(pos);
}

void SubNode::map_cb(nav_msgs::msg::OccupancyGrid::ConstSharedPtr map)
{
  uint32_t width = map->info.width;
  uint32_t height = map->info.height;
  double resolution = (double)(map->info.resolution);
  double origin_x = map->info.origin.position.x;
  double origin_y = map->info.origin.position.y;

  //RCLCPP_INFO(this->get_logger(), "Received map, width: %d, Height: %d", width, height);

  cv::Mat mat(height, width, CV_8UC1);

  for (uint32_t i = 0; i < height; ++i) {
    for (uint32_t j = 0; j < width; ++j) {
      uint32_t index = i * width + j;
      int occupancy_value = map->data[index];

      // Map occupancy values to pixel intensities (0-255)
      if (occupancy_value == -1) {
        mat.at<uchar>(i, j) = 128;  // Unknown (gray)
      } else if (occupancy_value == 0) {
        mat.at<uchar>(i, j) = 255;  // Free (white)
      } else {
        mat.at<uchar>(i, j) = 0;  // Occupied (black)
      }
    }
  }
  cv::Mat rgb_mat;
  cvtColor(mat, rgb_mat, CV_GRAY2RGB);
  map_manager_->update_static(rgb_mat, resolution, origin_x, origin_y);
}

}  // namespace qrb_ros::sample::inventory_scan
