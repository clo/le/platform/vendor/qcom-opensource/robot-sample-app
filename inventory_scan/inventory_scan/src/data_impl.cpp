// Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause-Clear

#include <algorithm>
#include <iostream>

#include "data_def.hpp"

using namespace std::placeholders;

namespace qrb_ros::sample::inventory_scan
{
//------------- Class PathManager ---------------------- //
bool PathManager::add_station(uint32_t id, const Point2D & pos)
{
  lock_.lock();
  auto ret = station_.insert({ id, pos });
  lock_.unlock();

  std::cout << "add station: " << id <<std::endl;

  if (!ret.second)
    return false;
  else
    return true;
}

void PathManager::get_station(uint32_t id, Point2D& pos)
{
  lock_.lock();
  pos = station_.at(id);
  lock_.unlock();
}

bool PathManager::delete_station(uint32_t id)
{
  lock_.lock();
  auto ret = station_.erase(id);
  lock_.unlock();

  if (ret)
    return true;
  else
    return false;
}

void PathManager::get_stations(std::unordered_map<uint32_t, Point2D> & station)
{
  lock_.lock();
  station = std::unordered_map<uint32_t, Point2D>(station_.cbegin(), station_.cend());
  lock_.unlock();
}

bool PathManager::add_path(uint32_t id1, uint32_t id2)
{
  bool ret = false;
  if(id1 == 0 || id2 == 0)
    return false;

  //std::cout << "add path:"<< id1 << "-->" << id2 <<std::endl;
  lock_.lock();
  auto it = path_.find(id1);
  if (it == path_.end()) {
    path_.insert({ id1, std::vector<uint32_t>{id2} });
    ret = true;
  } else {
    auto it2 = std::find(it->second.begin(), it->second.end(), id2);
    if (it2 == it->second.end()) {
      it->second.push_back(id2);
      ret = true;
    } else {
      ret = false;
    }
  }
  lock_.unlock();

  return ret;
}

bool PathManager::delete_path(uint32_t id1, uint32_t id2)
{
  bool ret = false;
  lock_.lock();
  auto it = path_.find(id1);
  if (it == path_.end()) {
    ret = false;
  } else {
    auto it2 = std::find(it->second.begin(), it->second.end(), id2);
    if (it2 != it->second.end()) {
      it->second.erase(it2);
      ret = true;
    } else {
      ret = false;
    }
  }
  lock_.unlock();

  return ret;
}

void PathManager::get_path(std::unordered_map<uint32_t, std::vector<uint32_t>> & path)
{
  lock_.lock();
  path = std::unordered_map<uint32_t, std::vector<uint32_t>>(path_.cbegin(), path_.cend());
  lock_.unlock();
}

bool PathManager::add_full_path(const std::vector<uint32_t> & path)
{
  lock_.lock();
  full_path_ = path;
  lock_.unlock();

  return true;
}

void PathManager::get_full_path(std::vector<uint32_t> & full_path)
{
  lock_.lock();
  full_path = std::vector<uint32_t>(full_path_.cbegin(), full_path_.cend());
  lock_.unlock();
}

//------------- Class NaviState ---------------------- //
NaviState::NaviState() : state_(NaviState::IDLE)
{}

uint32_t NaviState::get_state(void)
{
  uint32_t state;
  lock_.lock();
  state = state_;
  lock_.unlock();
  return state;
}

void NaviState::set_state(uint32_t state)
{
  lock_.lock();
  state_ = state;
  lock_.unlock();
}

//------------- Class SyncState ---------------------- //
void SyncState::reset(void)
{
  std::unique_lock<std::mutex> lck(lock_);
  state_ = SyncState::WAITING;
}

void SyncState::wait_start(void)
{
  std::unique_lock<std::mutex> lck(lock_);
  while (state_ == SyncState::WAITING) { cv_.wait(lck); }
}

void SyncState::start(void)
{
  std::unique_lock<std::mutex> lck(lock_);
  state_ = SyncState::START;
  cv_.notify_all();
}

void SyncState::wait_stop(void)
{
  std::unique_lock<std::mutex> lck(lock_);
  while (state_ == SyncState::START) { cv_.wait(lck); }
}

void SyncState::stop(void)
{
  std::unique_lock<std::mutex> lck(lock_);
  state_ = SyncState::STOP;
  cv_.notify_all();
}

void SyncState::release(void)
{
  std::unique_lock<std::mutex> lck(lock_);
  state_ = SyncState::RELEASE;
  cv_.notify_all();
}

uint32_t SyncState::get_state(void)
{
  uint32_t state;
  lock_.lock();
  state = state_;
  lock_.unlock();
  return state;
}

//------------- Class GoodsManager ---------------------- //
GoodsManager::GoodsManager() : cur_shelf_(-1)
{}

void GoodsManager::process_msg(const std::string& msg, bool is_goods)
{
  if(is_goods) {
    // QR_ID, name:xxx, q:num [first: last, last:]
    std::string::size_type name_s = msg.find_first_of(":");
    std::string::size_type name_e = msg.find_last_of(",");
    if(name_s >= name_e) return;

    std::string name = msg.substr(name_s + 1, name_e - name_s - 1);

    auto q_s = msg.find_last_of(":");
    if(q_s == std::string::npos) return;
    std::string quantity_s = msg.substr(q_s + 1);
    //std::cout << "quantity_s:" << quantity_s <<std::endl;
    int32_t quantity_n = std::stoi(quantity_s);

    //update msgs
    lock_.lock();
    auto it = goods_.begin();
    for(; it != goods_.end(); ++it) {
      if(name.compare(it->name) == 0) {
        it->quantity += (uint32_t)quantity_n;
        break;
      }
    }
    if(it == goods_.end()) {
      GoodsInfo goods;
      goods.name = name;
      goods.quantity = (uint32_t)quantity_n;

      goods_.push_back(goods);
    }

    if(cur_shelf_ != -1) {
      if(name.compare(cur_name_)) {
        std::string warning = "shelf #" + std::to_string(cur_shelf_) + " place " +
	  cur_name_ + ", but there are " + quantity_s + " " + name + " on it.";
        warning_msgs_.push_back(warning);
      }
    }

    lock_.unlock();

  } else {
    // shelf:ID, name:xxx [first: first, last:]
    std::string::size_type id_s = msg.find_first_of(":");
    std::string::size_type id_e = msg.find_first_of(",");
    if(id_s >= id_e) return;

    std::string id_str = msg.substr(id_s + 1, id_e - id_s - 1);
    int32_t id = std::stoi(id_str);

    auto name_s = msg.find_last_of(":");
    if(name_s == std::string::npos) return;
    std::string name = msg.substr(name_s + 1);

    lock_.lock();
    cur_name_ = name;
    cur_shelf_ = id;
    lock_.unlock();
  }
}

void GoodsManager::get_goods_info(std::vector<struct GoodsInfo>& goods,
    std::vector<std::string>& warn_msg)
{
  lock_.lock();
  goods = std::vector<struct GoodsInfo>(goods_.cbegin(), goods_.cend());
  warn_msg = std::vector<std::string>(warning_msgs_.cbegin(), warning_msgs_.cend());
  lock_.unlock();
}

//------------- Class MapManager ---------------------- //
MapManager::MapManager(std::shared_ptr<PathManager> path) : path_(path) {}

void MapManager::update_static(const cv::Mat & map, double res, double x, double y)
{
  lock_.lock();
  map.copyTo(static_map_);
  resolution_ = res;
  origin_x_ = x;
  origin_y_ = y;
  lock_.unlock();
}

bool MapManager::draw_map(cv::Mat & map)
{
  bool ret;
  double res;
  double x;
  double y;
  Point2D cur_pose;
  cv::Point pixel1;
  cv::Point pixel2;
  Point2D point1;
  Point2D point2;

  lock_.lock();
  if (static_map_.empty())
    ret = false;
  else {
    static_map_.copyTo(map);
    res = resolution_;
    x = origin_x_;
    y = origin_y_;
    cur_pose = cur_pose_;
    ret = true;
  }
  lock_.unlock();

  if (ret) {
    // 1: draw cur point
    pose_to_pixel(cur_pose, pixel1, res, x, y, map);
    if (is_pixel_in_image(pixel1, map)) {
      cv::circle(map, pixel1, 3, cv::Scalar(0, 255, 0));
    }

    // 2: draw stations
    std::unordered_map<uint32_t, Point2D> station;
    path_->get_stations(station);
    auto st_it = station.cbegin();
    while (st_it != station.cend()) {
      point1 = st_it->second;
      pose_to_pixel(point1, pixel1, res, x, y, map);
      if (is_pixel_in_image(pixel1, map)) {
        cv::circle(map, pixel1, 3, cv::Scalar(0, 0, 255));
	      cv::putText(map, std::to_string(st_it->first), pixel1, 
          cv::FONT_HERSHEY_PLAIN, 1.0, cv::Scalar(0, 0, 255));
      }

      ++st_it;
    }

    // 3:draw path
    std::unordered_map<uint32_t, std::vector<uint32_t>> path;
    path_->get_path(path);
    auto pt_it = path.cbegin();
    while (pt_it != path.cend()) {
      point1 = station.at(pt_it->first);
      pose_to_pixel(point1, pixel1, res, x, y, map);
      std::vector<uint32_t>::const_iterator vec_it = pt_it->second.cbegin();
      while (vec_it != pt_it->second.cend()) {
        point2 = station.at(*vec_it);
        pose_to_pixel(point2, pixel2, res, x, y, map);
        if (is_pixel_in_image(pixel1, map) && is_pixel_in_image(pixel2, map)){
          cv::line(map, pixel1, pixel2, cv::Scalar(0, 0, 255));
        }
        ++vec_it;
      }
      ++pt_it;
    }

    // 4: draw full path
    std::vector<uint32_t> full_path;
    path_->get_full_path(full_path);
    std::vector<uint32_t>::const_iterator fp_it = full_path.cbegin();
    while ((fp_it != full_path.cend()) && ((fp_it + 1) != full_path.cend())) {
      point1 = station.at(*fp_it);
      point2 = station.at(*(fp_it + 1));
      pose_to_pixel(point1, pixel1, res, x, y, map);
      pose_to_pixel(point2, pixel2, res, x, y, map);

      if (is_pixel_in_image(pixel1, map) && is_pixel_in_image(pixel2, map)){
        cv::line(map, pixel1, pixel2, cv::Scalar(0, 255, 0));
      }

      fp_it++;
    }
  }

  return ret;
}

void MapManager::set_pose(const Point2D & pose)
{
  lock_.lock();
  cur_pose_ = pose;
  lock_.unlock();
}

void MapManager::pose_to_pixel(const Point2D & pose,
    cv::Point & pixel,
    double res,
    double ori_x,
    double ori_y,
    const cv::Mat & map)
{
  double x = pose.x - ori_x;
  double y = pose.y - ori_y;

  // point to pixel in image
  pixel.x = static_cast<int>(x / res + 0.5);
  //pixel.y = map.rows - static_cast<int>(y / res + 0.5);
  pixel.y = static_cast<int>(y / res + 0.5);
}

bool MapManager::is_pixel_in_image(const cv::Point & pixel, const cv::Mat & image)
{
  // Check if pixel coordinates are within image boundaries
  return pixel.x >= 0 && pixel.x < image.cols && pixel.y >= 0 && pixel.y < image.rows;
}

}  // namespace qrb_ros::sample::inventory_scan
