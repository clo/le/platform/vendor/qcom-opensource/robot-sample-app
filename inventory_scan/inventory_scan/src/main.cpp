// Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause-Clear

#include "nodes_def.hpp"

using namespace qrb_ros::sample::inventory_scan;

int main(int argc, char ** argv)
{
  rclcpp::init(argc, argv);

  std::shared_ptr<PathManager> path_manager = std::make_shared<PathManager>();
  std::shared_ptr<MapManager> map_manager = std::make_shared<MapManager>(path_manager);
  std::shared_ptr<NaviState> navi_state = std::make_shared<NaviState>();
  std::shared_ptr<SyncState> sync_state = std::make_shared<SyncState>();
  std::shared_ptr<GoodsManager> goods = std::make_shared<GoodsManager>();

  auto path_node = std::make_shared<PathManagerNode>(path_manager, navi_state);
  auto scan_node = std::make_shared<ScanNode>(sync_state, goods);
  auto navi_node = std::make_shared<NaviNode>(navi_state,  path_manager, sync_state);
  auto pub_node = std::make_shared<PubNode>(map_manager, navi_state, goods);
  auto sub_node = std::make_shared<SubNode>(map_manager);

  rclcpp::executors::MultiThreadedExecutor executor;
  executor.add_node(path_node);
  executor.add_node(scan_node);
  executor.add_node(navi_node);
  executor.add_node(pub_node);
  executor.add_node(sub_node);
  executor.spin();

  rclcpp::shutdown();
  std::this_thread::sleep_for(std::chrono::seconds(1));
  sync_state->release();
  std::this_thread::sleep_for(std::chrono::seconds(1));
  return 0;
}
