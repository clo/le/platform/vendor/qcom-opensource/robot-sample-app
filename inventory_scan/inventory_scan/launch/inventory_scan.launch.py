# Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
# SPDX-License-Identifier: BSD-3-Clause-Clear

import launch
import launch_ros

def generate_launch_description():
    scan_node = launch_ros.actions.Node(
        package='inventory_scan',
        executable='scan_node',
        output='screen',
        parameters=[{'use_sim_time': True}]
        )
    return launch.LaunchDescription([
        scan_node,
    ])
