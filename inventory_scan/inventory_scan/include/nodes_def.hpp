// Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause-Clear

#ifndef QRB_ROS_SAMPLE_INVENTORY_SCAN_NODES_HPP_
#define QRB_ROS_SAMPLE_INVENTORY_SCAN_NODES_HPP_

#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <tf2_ros/buffer.h>
#include <tf2_ros/transform_listener.h>

#include <geometry_msgs/msg/transform_stamped.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <rclcpp/rclcpp.hpp>
#include <rclcpp_action/rclcpp_action.hpp>
#include <std_msgs/msg/string.hpp>

#include "data_def.hpp"
#include "inventory_scan_msgs/srv/scan_path.hpp"
#include "inventory_scan_msgs/srv/scan_control.hpp"
#include "inventory_scan_msgs/msg/scan_result.hpp"
#include "nav_msgs/msg/occupancy_grid.hpp"
#include "qrb_ros_amr_msgs/action/cmd.hpp"
#include "qrb_ros_navigation_msgs/srv/compute_follow_path.hpp"
#include "qrb_ros_navigation_msgs/srv/compute_p2p_path.hpp"
#include "qrb_ros_navigation_msgs/srv/virtual_path.hpp"
#include "qrb_ros_vision_service_msgs/action/qr_code_case.hpp"

namespace qrb_ros::sample::inventory_scan
{
class PathManagerNode : public rclcpp::Node
{
public:
  using PathService = inventory_scan_msgs::srv::ScanPath;
  using VirtualPath = qrb_ros_navigation_msgs::srv::VirtualPath;
  using VirtualPathFuture = rclcpp::Client<VirtualPath>::SharedFuture;
  explicit PathManagerNode(std::shared_ptr<PathManager> path,
    std::shared_ptr<NaviState> navi_state);
  ~PathManagerNode() = default;

private:
  std::shared_ptr<PathManager> path_manager_;
  std::shared_ptr<NaviState> navi_state_;
  rclcpp::Service<PathService>::SharedPtr server_{ nullptr };
  // virtual path service client
  rclcpp::Client<VirtualPath>::SharedPtr vp_client_{ nullptr };
  std::unique_ptr<tf2_ros::Buffer> tf_buffer_;
  std::shared_ptr<tf2_ros::TransformListener> tf_listener_;
  rclcpp::TimerBase::SharedPtr timer_;
  rclcpp::CallbackGroup::SharedPtr callback_group_;
  std::mutex lock_;
  std::condition_variable cv_;
  uint8_t vp_response_ret_;

  void service_callback(const std::shared_ptr<rmw_request_id_t> request_header,
      const std::shared_ptr<PathService::Request> request,
      std::shared_ptr<PathService::Response> response);

  void init_station_list(std::vector<uint32_t>& list, size_t& len);
  void init_station(const std::vector<uint32_t>& list, size_t len);
  void init_path(const std::vector<uint32_t>& list, size_t len);
  uint32_t virtual_path_add_station(const Point2D& pos);
  bool virtual_path_delete_station(uint32_t id);
  bool query_pose(Point2D& pos);
  bool virtual_path_add_path(uint32_t id1, uint32_t id2);
  bool virtual_path_delete_path(uint32_t id1, uint32_t id2);

  void init();
};

class ScanNode : public rclcpp::Node
{
public:
  using VisionAction = qrb_ros_vision_service_msgs::action::QRCodeCase;
  using GoalHandleVision = rclcpp_action::ClientGoalHandle<VisionAction>;
  explicit ScanNode(std::shared_ptr<SyncState> sync_state,
    std::shared_ptr<GoodsManager> goods);

private:
  rclcpp_action::Client<VisionAction>::SharedPtr client_{ nullptr };

  void goal_response_callback(GoalHandleVision::SharedPtr handle);
  void feedback_callback(GoalHandleVision::SharedPtr handle,
      const std::shared_ptr<const VisionAction::Feedback> feedback);
  void result_callback(const GoalHandleVision::WrappedResult& result);

  void execute(void);
  std::shared_ptr<SyncState> sync_state_;
  std::shared_ptr<GoodsManager> goods_;
  GoalHandleVision::SharedPtr goal_handle_;
};

class NaviNode : public rclcpp::Node
{
public:
  const static uint32_t NAVI_IDLE = 0;
  const static uint32_t NAVI_START = 1;
  const static uint32_t NAVI_FINISH = 2;
  const static uint32_t NAVI_ERROR = 3;

  using NaviService = inventory_scan_msgs::srv::ScanControl;

  // navigation includes p2p and followpth
  using NavigateToPose = qrb_ros_amr_msgs::action::Cmd;
  using GoalHandleNavigate = rclcpp_action::ClientGoalHandle<NavigateToPose>;

  using ComputeFollowPath = qrb_ros_navigation_msgs::srv::ComputeFollowPath;
  using ComputeP2pPath = qrb_ros_navigation_msgs::srv::ComputeP2pPath;

  explicit NaviNode(std::shared_ptr<NaviState> navi_state, std::shared_ptr<PathManager> path,
    std::shared_ptr<SyncState> sync_state);

private:
  // navi service for web UI
  rclcpp::Service<NaviService>::SharedPtr navi_server_{ nullptr };
  void service_callback(const std::shared_ptr<rmw_request_id_t> request_header,
      const std::shared_ptr<NaviService::Request> request,
      std::shared_ptr<NaviService::Response> response);

  void execute(const std::vector<uint32_t>& full_path);
  void go_home();
  // for Navigation Action client
  rclcpp_action::Client<NavigateToPose>::SharedPtr navi_client_{ nullptr };
  GoalHandleNavigate::SharedPtr goal_handle_;
  void navi_goal_response_callback(GoalHandleNavigate::SharedPtr goal_handle);
  void navi_feedback_callback(GoalHandleNavigate::SharedPtr,
      const std::shared_ptr<const NavigateToPose::Feedback> feedback);
  void navi_result_callback(const GoalHandleNavigate::WrappedResult & result);
  void init();

  // compute path service client
  rclcpp::Client<ComputeFollowPath>::SharedPtr fp_client_{ nullptr };
  rclcpp::Client<ComputeP2pPath>::SharedPtr p2p_client_{ nullptr };

  rclcpp::TimerBase::SharedPtr timer_;
  rclcpp::CallbackGroup::SharedPtr callback_group_;

  std::shared_ptr<NaviState> navi_state_;
  std::shared_ptr<PathManager> path_;
  std::shared_ptr<SyncState> sync_state_;

  uint32_t navi_result_;
  bool navi_server_ready_;
  std::mutex lock_;
  std::condition_variable cv_;
};

class PubNode : public rclcpp::Node
{
public:
  explicit PubNode(std::shared_ptr<MapManager> map_manager,
                   std::shared_ptr<NaviState> navi_state,
                   std::shared_ptr<GoodsManager> goods);

private:
  rclcpp::Publisher<inventory_scan_msgs::msg::ScanResult>::SharedPtr result_pub_;
  rclcpp::Publisher<sensor_msgs::msg::CompressedImage>::SharedPtr img_pub_;
  rclcpp::TimerBase::SharedPtr timer_{ nullptr };
  std::shared_ptr<MapManager> map_manager_;
  std::shared_ptr<NaviState> navi_state_;
  std::shared_ptr<GoodsManager> goods_;
  void publish_msgs();
  void timer_cb();
};

class SubNode : public rclcpp::Node
{
public:
  explicit SubNode(std::shared_ptr<MapManager> map_manager);

private:
  std::unique_ptr<tf2_ros::Buffer> tf_buffer_;
  std::shared_ptr<tf2_ros::TransformListener> tf_listener_;
  rclcpp::Subscription<nav_msgs::msg::OccupancyGrid>::SharedPtr map_listener_{ nullptr };
  rclcpp::TimerBase::SharedPtr tf_timer_{ nullptr };
  rclcpp::TimerBase::SharedPtr map_timer_{ nullptr };
  std::shared_ptr<MapManager> map_manager_;
  void tf_timer_cb();
  void map_cb(nav_msgs::msg::OccupancyGrid::ConstSharedPtr map);
};

}  // namespace qrb_ros::sample::inventory_scan

#endif  // QRB_ROS_SAMPLE_INVENTORY_SCAN_NODES_HPP_
