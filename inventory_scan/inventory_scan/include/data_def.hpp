// Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause-Clear

#ifndef QRB_ROS_SAMPLE_INVENTORY_SCAN_DATA_HPP_
#define QRB_ROS_SAMPLE_INVENTORY_SCAN_DATA_HPP_

#include <cstdint>
#include <memory>
#include <mutex>
#include <unordered_map>
#include <utility>
#include <vector>
#include <condition_variable>

#include "opencv2/opencv.hpp"

namespace qrb_ros::sample::inventory_scan
{
struct Point2D
{
  double x;
  double y;
  double angle;
};

struct GoodsInfo
{
  std::string name;
  uint32_t quantity;
};

class PathManager
{
public:
  using Point2D = struct Point2D;

  PathManager() = default;
  bool add_station(uint32_t id, const Point2D& pos);
  void get_station(uint32_t id, Point2D& pos);
  bool delete_station(uint32_t id);
  void get_stations(std::unordered_map<uint32_t, Point2D>& station);
  bool add_path(uint32_t id1, uint32_t id2);
  bool delete_path(uint32_t id1, uint32_t id2);
  void get_path(std::unordered_map<uint32_t, std::vector<uint32_t>> & path);
  bool add_full_path(const std::vector<uint32_t> & path);
  void get_full_path(std::vector<uint32_t>& full_path);
  ~PathManager() = default;

private:
  std::unordered_map<uint32_t, Point2D> station_;
  std::unordered_map<uint32_t, std::vector<uint32_t>> path_;
  std::vector<uint32_t> full_path_;
  std::mutex lock_;
};

class NaviState
{
public:
  const static uint32_t IDLE = 0;
  const static uint32_t NAVIGATING = 1;
  const static uint32_t GOING_HOME = 2;
  const static uint32_t NAVI_ERR = 3;

  NaviState();
  uint32_t get_state(void);
  void set_state(uint32_t state);
  ~NaviState() = default;

private:
  uint32_t state_;
  std::mutex lock_;
};

class SyncState
{
public:
  const static uint32_t WAITING = 0;
  const static uint32_t START = 1;
  const static uint32_t STOP = 2;
  const static uint32_t RELEASE = 3;

  SyncState() = default;
  void reset(void);
  void wait_start(void);
  void start(void);
  void wait_stop(void);
  void stop(void);
  void release(void);
  uint32_t get_state(void);
  ~SyncState() = default;

private:
  uint32_t state_;
  std::mutex lock_;
  std::condition_variable cv_;
};

class GoodsManager
{
public:

  GoodsManager();
  void process_msg(const std::string& msg, bool is_goods);
  void get_goods_info(std::vector<struct GoodsInfo>& goods,
    std::vector<std::string>& warn_msg);
  ~GoodsManager() = default;

private:
  std::mutex lock_;
  std::vector<struct GoodsInfo> goods_;
  std::vector<std::string> warning_msgs_;
  int32_t cur_shelf_;
  std::string cur_name_;
};

class MapManager
{
public:
  using Point2D = struct Point2D;
  MapManager(std::shared_ptr<PathManager> path);
  void update_static(const cv::Mat & map, double res, double x, double y);
  void set_pose(const Point2D & pose);
  void pose_to_pixel(const Point2D & pose,
      cv::Point & pixel,
      double res,
      double x,
      double y,
      const cv::Mat & map);
  bool is_pixel_in_image(const cv::Point & pixel, const cv::Mat & image);
  bool draw_map(cv::Mat & map);
  ~MapManager() = default;

private:
  std::shared_ptr<PathManager> path_;
  cv::Mat static_map_;
  double resolution_;
  double origin_x_;
  double origin_y_;
  Point2D cur_pose_;
  std::mutex lock_;
};

}  // namespace qrb_ros::sample::inventory_scan

#endif  // QRB_ROS_SAMPLE_INVENTORY_SCAN_DATA_HPP_
